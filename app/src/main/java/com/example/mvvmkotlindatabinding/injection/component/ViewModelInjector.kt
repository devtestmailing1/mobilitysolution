package com.example.mvvmkotlindatabinding.injection.component

import com.example.mvvmkotlindatabinding.injection.module.NetworkModule
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(postListViewModel: PostListViewModel)

    @Component.Builder
    interface Builder{
        fun build():ViewModelInjector
        fun networkModule(networkModule:NetworkModule):Builder
    }
}