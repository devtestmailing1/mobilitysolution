package com.example.mvvmkotlindatabinding.injection

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(private var numberOfItem:String):ViewModelProvider.Factory {
    override fun <T:ViewModel?> create(modelClass:Class<T>):T{
        if (modelClass.isAssignableFrom(PostListViewModel::class.java)){
         @Suppress("UNCHECKED_CAST")
         return PostListViewModel(numberOfItems = numberOfItem) as T
        }
        throw IllegalArgumentException("unknown ViewModel class")
    }
}