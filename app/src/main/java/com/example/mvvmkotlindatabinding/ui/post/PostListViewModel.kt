package com.example.mvvmkotlindatabinding.ui.post


import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.model.DataModel
import com.example.mvvmkotlindatabinding.network.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

class PostListViewModel(var numberOfItems:String):BaseViewModel() {
    @Inject
    lateinit var postApi: PostApi

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts(numberOfItems) }
    val postListAdapter:PostListAdapter = PostListAdapter()
    private lateinit var subscription:Disposable
    private var disposable:CompositeDisposable

    init {
        loadPosts(numberOfItems)
        disposable = CompositeDisposable()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    public fun loadPosts(items: String){

        postApi.getListData(items).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{onRetrievePostListStart()}
            .doOnTerminate{onRetrievePostListFinish()}
            .subscribe(object : ResponseObserver<DataModel>() {
                override fun onNetworkError(e: Throwable?) {
                    Log.e("Response1>>>", ">>>")
                    onRetrievePostListError()
                }
                override fun onServerError(e: Throwable?, code: Int) {
                    Log.e("Response2>>>", ">>>")
                    onRetrievePostListError()
                }
                override fun onNext(t: Response<DataModel>) {
                   Log.e("urlll>",t.raw().request.url.toString())
                    Log.e("Response3>>>", ">>>"+t.body()?.results?.size+"")
                    onRetrievePostListSuccess(t.body()?.results!!)
                }
                override fun onNoData() {
                    Log.e("Response4>>>", ">>>")
                }
            })


    }



    private fun onRetrievePostListStart(){
        loadingVisibility.value=View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE

    }

    private fun onRetrievePostListSuccess(results: MutableList<DataModel.Result>) {
        postListAdapter.updatePostList(results)

    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }


}