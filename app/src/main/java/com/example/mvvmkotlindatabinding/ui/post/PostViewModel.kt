package com.example.mvvmkotlindatabinding.ui.post


import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.model.DataModel

class PostViewModel:BaseViewModel() {
    private val name = MutableLiveData<String>()
    private val email = MutableLiveData<String>()
    private val phone = MutableLiveData<String>()
    private val Location = MutableLiveData<DataModel.Location>()

    fun bind(post: DataModel.Result){
        name.value = post.name.first
        email.value = post.email
        phone.value = post.phone
        Location.value = post.location
    }

    fun getPostName():MutableLiveData<String>{
        return name
    }

    fun getPostEmail():MutableLiveData<String>{
        return email
    }

    fun getPostPhone():MutableLiveData<String>{
        return phone
    }

    fun getPostClick():MutableLiveData<DataModel.Location>{
        return Location
    }



}