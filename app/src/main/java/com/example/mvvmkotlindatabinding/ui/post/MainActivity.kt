package com.example.mvvmkotlindatabinding.ui.post

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.fragment.HomeFragment
import com.example.mvvmkotlindatabinding.fragment.DetailsFragment
import com.example.mvvmkotlindatabinding.model.DataModel


class MainActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.mycontainer, HomeFragment(),"Homee").addToBackStack(HomeFragment().javaClass.name).commit()
    }

    fun GoToNextFragment(dataModel: DataModel.Location){
        val fragment = DetailsFragment()
        val bundle = Bundle()
        bundle.putString("city", dataModel.city)
        bundle.putString("state",dataModel.state)
        bundle.putString("country",dataModel.country)
        bundle.putInt("postcode",dataModel.postcode)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.mycontainer,fragment).addToBackStack(fragment.javaClass.name).commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }

    }
}