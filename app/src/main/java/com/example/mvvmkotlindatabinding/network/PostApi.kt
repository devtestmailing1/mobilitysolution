package com.example.mvvmkotlindatabinding.network

import com.example.mvvmkotlindatabinding.model.DataModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PostApi {

    /*@GET("api")
    fun getListData(@Query("results") page: Int): Observable<Response<List<Json4Kotlin_Base>>>*/

    @GET("api?")
    fun getListData(@Query("results") page: String): Observable<Response<DataModel>>
}