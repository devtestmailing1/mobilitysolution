package com.example.mvvmkotlindatabinding.base

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlindatabinding.injection.component.DaggerViewModelInjector
import com.example.mvvmkotlindatabinding.injection.component.ViewModelInjector
import com.example.mvvmkotlindatabinding.injection.module.NetworkModule
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel

abstract class BaseViewModel:ViewModel() {

    private val injector : ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject(){
        when(this){
            is PostListViewModel -> injector.inject(this)
        }
    }


}