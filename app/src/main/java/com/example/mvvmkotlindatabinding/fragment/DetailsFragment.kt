package com.example.mvvmkotlindatabinding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mvvmkotlindatabinding.R
import kotlinx.android.synthetic.main.details_fragment.*

class DetailsFragment : Fragment(){

    private lateinit var city:String
    private lateinit var state:String
    private lateinit var country:String
    private lateinit var postCode:String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.details_fragment, container, false)
        return view
    }

    override fun onResume() {
        super.onResume()
        getLocationData()
    }

    fun getLocationData(){
        city = arguments?.get("city") as String
        state = arguments?.get("state") as String
        country = arguments?.get("country") as String
        city1.text = city
        state1.text = state
        country1.text = country
    }

}