package com.example.mvvmkotlindatabinding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.databinding.HomeFragmentBinding
import com.example.mvvmkotlindatabinding.injection.ViewModelFactory
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : Fragment(),SwipeRefreshLayout.OnRefreshListener {


    private lateinit var viewModel: PostListViewModel
    private var errorSnackbar: Snackbar? = null
    private lateinit var binding: HomeFragmentBinding
    lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    var numberOfItems:Int = 5

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeRefreshLayout()
    }


    private fun swipeRefreshLayout(){
        registerViewModels()
        mSwipeRefreshLayout = swipe_container as SwipeRefreshLayout
        mSwipeRefreshLayout.setOnRefreshListener(this)
        mSwipeRefreshLayout.setColorSchemeResources(
            android.R.color.holo_red_light,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark,
            android.R.color.holo_blue_dark)

        mSwipeRefreshLayout.post {

        }

    }


    private fun registerViewModels() {
        binding.postList.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        viewModel =
            ViewModelProviders.of(this, ViewModelFactory(numberOfItem = numberOfItems.toString())).get(PostListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else
                hideError()
        })
        binding.viewModel = viewModel



    }


    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

    override fun onRefresh() {
        numberOfItems = numberOfItems +3
        viewModel.loadPosts(numberOfItems.toString())
        mSwipeRefreshLayout.isRefreshing = false
    }


}